EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x19_Odd_Even J1
U 1 1 60885331
P 2200 2900
F 0 "J1" H 2250 4017 50  0000 C CNN
F 1 "Conn_02x19_Odd_Even" H 2250 3926 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x19_P2.54mm_Vertical" H 2200 2900 50  0001 C CNN
F 3 "~" H 2200 2900 50  0001 C CNN
	1    2200 2900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x19_Odd_Even J2
U 1 1 608867A0
P 5200 2900
F 0 "J2" H 5250 4017 50  0000 C CNN
F 1 "Conn_02x19_Odd_Even" H 5250 3926 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x19_P2.54mm_Vertical" H 5200 2900 50  0001 C CNN
F 3 "~" H 5200 2900 50  0001 C CNN
	1    5200 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2000 2000 1500 2000
Wire Wire Line
	1500 2100 2000 2100
Wire Wire Line
	2000 2200 1500 2200
Wire Wire Line
	1500 2300 2000 2300
Wire Wire Line
	2000 2400 1500 2400
Wire Wire Line
	1500 2500 2000 2500
Wire Wire Line
	2000 2600 1500 2600
Wire Wire Line
	1500 2700 2000 2700
Wire Wire Line
	2000 2800 1500 2800
Wire Wire Line
	1500 2900 2000 2900
Wire Wire Line
	2000 3000 1500 3000
Wire Wire Line
	1500 3100 2000 3100
Wire Wire Line
	2000 3200 1500 3200
Wire Wire Line
	1500 3300 2000 3300
Wire Wire Line
	2000 3400 1500 3400
Wire Wire Line
	1500 3500 2000 3500
Wire Wire Line
	2000 3600 1500 3600
Wire Wire Line
	1500 3700 2000 3700
Wire Wire Line
	2000 3800 1500 3800
Wire Wire Line
	3000 2000 2500 2000
Wire Wire Line
	2500 2100 3000 2100
Wire Wire Line
	3000 2200 2500 2200
Wire Wire Line
	2500 2300 3000 2300
Wire Wire Line
	3000 2400 2500 2400
Wire Wire Line
	2500 2500 3000 2500
Wire Wire Line
	3000 2600 2500 2600
Wire Wire Line
	2500 2700 3000 2700
Wire Wire Line
	3000 2800 2500 2800
Wire Wire Line
	2500 2900 3000 2900
Wire Wire Line
	3000 3000 2500 3000
Wire Wire Line
	2500 3100 3000 3100
Wire Wire Line
	3000 3200 2500 3200
Wire Wire Line
	2500 3300 3000 3300
Wire Wire Line
	3000 3400 2500 3400
Wire Wire Line
	2500 3500 3000 3500
Wire Wire Line
	3000 3600 2500 3600
Wire Wire Line
	2500 3700 3000 3700
Wire Wire Line
	3000 3800 2500 3800
Wire Wire Line
	5000 2000 4500 2000
Wire Wire Line
	4500 2100 5000 2100
Wire Wire Line
	5000 2200 4500 2200
Wire Wire Line
	4500 2300 5000 2300
Wire Wire Line
	5000 2400 4500 2400
Wire Wire Line
	4500 2500 5000 2500
Wire Wire Line
	5000 2600 4500 2600
Wire Wire Line
	4500 2700 5000 2700
Wire Wire Line
	5000 2800 4500 2800
Wire Wire Line
	4500 2900 5000 2900
Wire Wire Line
	5000 3000 4500 3000
Wire Wire Line
	4500 3100 5000 3100
Wire Wire Line
	5000 3200 4500 3200
Wire Wire Line
	4500 3300 5000 3300
Wire Wire Line
	5000 3400 4500 3400
Wire Wire Line
	4500 3500 5000 3500
Wire Wire Line
	5000 3600 4500 3600
Wire Wire Line
	4500 3700 5000 3700
Wire Wire Line
	5000 3800 4500 3800
Wire Wire Line
	6000 2000 5500 2000
Wire Wire Line
	5500 2100 6000 2100
Wire Wire Line
	6000 2200 5500 2200
Wire Wire Line
	5500 2300 6000 2300
Wire Wire Line
	6000 2400 5500 2400
Wire Wire Line
	5500 2500 6000 2500
Wire Wire Line
	6000 2600 5500 2600
Wire Wire Line
	5500 2700 6000 2700
Wire Wire Line
	6000 2800 5500 2800
Wire Wire Line
	5500 2900 6000 2900
Wire Wire Line
	6000 3000 5500 3000
Wire Wire Line
	5500 3100 6000 3100
Wire Wire Line
	6000 3200 5500 3200
Wire Wire Line
	5500 3300 6000 3300
Wire Wire Line
	6000 3400 5500 3400
Wire Wire Line
	5500 3500 6000 3500
Wire Wire Line
	6000 3600 5500 3600
Wire Wire Line
	5500 3700 6000 3700
Wire Wire Line
	6000 3800 5500 3800
Text Label 1600 2000 0    50   ~ 0
PC10
Text Label 1600 2100 0    50   ~ 0
PC12
Text Label 1600 2300 0    50   ~ 0
BOOT0
NoConn ~ 1500 2400
NoConn ~ 1500 2500
NoConn ~ 3000 2400
NoConn ~ 3000 3200
Text Label 1600 2600 0    50   ~ 0
PA13
Text Label 1600 2700 0    50   ~ 0
PA14
Text Label 1600 2800 0    50   ~ 0
PA15
$Comp
L power:GND #PWR?
U 1 1 608CAD14
P 1500 2900
F 0 "#PWR?" H 1500 2650 50  0001 C CNN
F 1 "GND" V 1505 2772 50  0000 R CNN
F 2 "" H 1500 2900 50  0001 C CNN
F 3 "" H 1500 2900 50  0001 C CNN
	1    1500 2900
	0    1    1    0   
$EndComp
Text Label 1600 3000 0    50   ~ 0
PB7
Text Label 1600 3100 0    50   ~ 0
PC13
Text Label 1600 3200 0    50   ~ 0
PC14
Text Label 1600 3300 0    50   ~ 0
PC15
Text Label 1600 3400 0    50   ~ 0
PD0
Text Label 1600 3500 0    50   ~ 0
PD1
$Comp
L power:+BATT #PWR?
U 1 1 608CBFE9
P 1500 3600
F 0 "#PWR?" H 1500 3450 50  0001 C CNN
F 1 "+BATT" V 1515 3727 50  0000 L CNN
F 2 "" H 1500 3600 50  0001 C CNN
F 3 "" H 1500 3600 50  0001 C CNN
	1    1500 3600
	0    -1   -1   0   
$EndComp
$Comp
L power:VDD #PWR?
U 1 1 608D185E
P 1500 2200
F 0 "#PWR?" H 1500 2050 50  0001 C CNN
F 1 "VDD" V 1515 2327 50  0000 L CNN
F 2 "" H 1500 2200 50  0001 C CNN
F 3 "" H 1500 2200 50  0001 C CNN
	1    1500 2200
	0    -1   -1   0   
$EndComp
Text Label 1600 3700 0    50   ~ 0
PC2
Text Label 1600 3800 0    50   ~ 0
PC3
Text Label 2600 3800 0    50   ~ 0
PC0
Text Label 2600 3700 0    50   ~ 0
PC1
Text Label 2600 3600 0    50   ~ 0
PB0
Text Label 2600 3500 0    50   ~ 0
PA4
Text Label 2600 3400 0    50   ~ 0
PA1
Text Label 2600 3300 0    50   ~ 0
PA0
Text Label 2800 3100 0    50   ~ 0
VIN
$Comp
L power:GND #PWR?
U 1 1 608D35DD
P 3000 3000
F 0 "#PWR?" H 3000 2750 50  0001 C CNN
F 1 "GND" V 3005 2872 50  0000 R CNN
F 2 "" H 3000 3000 50  0001 C CNN
F 3 "" H 3000 3000 50  0001 C CNN
	1    3000 3000
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 608D38DC
P 3000 2900
F 0 "#PWR?" H 3000 2650 50  0001 C CNN
F 1 "GND" V 3005 2772 50  0000 R CNN
F 2 "" H 3000 2900 50  0001 C CNN
F 3 "" H 3000 2900 50  0001 C CNN
	1    3000 2900
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 608D3C30
P 3000 2800
F 0 "#PWR?" H 3000 2650 50  0001 C CNN
F 1 "+5V" V 3015 2928 50  0000 L CNN
F 2 "" H 3000 2800 50  0001 C CNN
F 3 "" H 3000 2800 50  0001 C CNN
	1    3000 2800
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 608D3FA6
P 3000 2700
F 0 "#PWR?" H 3000 2550 50  0001 C CNN
F 1 "+3V3" V 3015 2828 50  0000 L CNN
F 2 "" H 3000 2700 50  0001 C CNN
F 3 "" H 3000 2700 50  0001 C CNN
	1    3000 2700
	0    1    1    0   
$EndComp
Text Label 2600 2600 0    50   ~ 0
RESET
Text Label 2600 2500 0    50   ~ 0
IOREF
$Comp
L power:GND #PWR?
U 1 1 608D4ACE
P 3000 2300
F 0 "#PWR?" H 3000 2050 50  0001 C CNN
F 1 "GND" V 3005 2172 50  0000 R CNN
F 2 "" H 3000 2300 50  0001 C CNN
F 3 "" H 3000 2300 50  0001 C CNN
	1    3000 2300
	0    -1   -1   0   
$EndComp
Text Label 2800 2200 0    50   ~ 0
E5V
Text Label 2600 2100 0    50   ~ 0
PD2
Text Label 2600 2000 0    50   ~ 0
PC11
Text Label 4600 2000 0    50   ~ 0
PC9
Text Label 4600 2100 0    50   ~ 0
PB8
Text Label 4600 2200 0    50   ~ 0
PB9
Text Label 4800 2300 0    50   ~ 0
AVdd
$Comp
L power:GND #PWR?
U 1 1 608D6294
P 4500 2400
F 0 "#PWR?" H 4500 2150 50  0001 C CNN
F 1 "GND" V 4505 2272 50  0000 R CNN
F 2 "" H 4500 2400 50  0001 C CNN
F 3 "" H 4500 2400 50  0001 C CNN
	1    4500 2400
	0    1    1    0   
$EndComp
Text Label 4600 2500 0    50   ~ 0
PA5
Text Label 4600 2600 0    50   ~ 0
PA6
Text Label 4600 2700 0    50   ~ 0
PA7
Text Label 4600 2800 0    50   ~ 0
PB6
Text Label 4600 2900 0    50   ~ 0
PC7
Text Label 4600 3000 0    50   ~ 0
PA9
Text Label 4600 3100 0    50   ~ 0
PA8
Text Label 4600 3200 0    50   ~ 0
PB10
Text Label 4600 3300 0    50   ~ 0
PB4
Text Label 4600 3400 0    50   ~ 0
PB5
Text Label 4600 3500 0    50   ~ 0
PB3
Text Label 4600 3600 0    50   ~ 0
PA10
Text Label 4600 3700 0    50   ~ 0
PA2
Text Label 4600 3800 0    50   ~ 0
PA3
Text Label 5600 2000 0    50   ~ 0
PC8
Text Label 5600 2100 0    50   ~ 0
PC6
Text Label 5600 2200 0    50   ~ 0
PC5
Text Label 5800 2300 0    50   ~ 0
U5V
NoConn ~ 6000 2400
NoConn ~ 6000 3800
NoConn ~ 6000 3700
Text Label 5600 2500 0    50   ~ 0
PA12
Text Label 5600 2600 0    50   ~ 0
PA11
Text Label 5600 2700 0    50   ~ 0
PB12
Text Label 5600 2800 0    50   ~ 0
PB11
Text Label 5600 3000 0    50   ~ 0
PB2
Text Label 5600 3100 0    50   ~ 0
PB1
Text Label 5600 3200 0    50   ~ 0
PB15
Text Label 5600 3300 0    50   ~ 0
PB14
Text Label 5600 3400 0    50   ~ 0
PB13
Text Label 5600 3600 0    50   ~ 0
PC4
Text Label 5800 3500 0    50   ~ 0
AGND
Text Notes 1850 2250 0    25   ~ 0
(1)
Text Notes 1800 2550 0    25   ~ 0
(3)
Text Notes 1800 2650 0    25   ~ 0
(3)
Text Notes 2750 3650 0    25   ~ 0
(4)
Text Notes 2750 3750 0    25   ~ 0
(4)
Text Notes 5950 2250 0    25   ~ 0
(2)
Text Notes 1500 4500 0    50   ~ 0
1: Default state of BOOT0 is LOW\n2: U5V is 5V power from ST-Link USB connector\n3: Pins shared with the SWD/JTAG signals from ST-Link\n4: ADC pins via solder bridge
Text Notes 4050 3700 0    50   ~ 0
USART2_TX
Text Notes 4050 3800 0    50   ~ 0
USART2_RX
Text Notes 3750 3750 0    50   ~ 0
(Debug)
Text Notes 1000 3100 0    50   ~ 0
PUSH BTN.
Text Notes 4050 2500 0    50   ~ 0
USER LED
Text Notes 4750 3450 0    25   ~ 0
(3)
$EndSCHEMATC
